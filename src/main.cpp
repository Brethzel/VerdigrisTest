#include <QGuiApplication>
#include <QSurfaceFormat>
#include <QtQml>

#include <config.h>

#include "internal/AutoRefreshView.h"
#include "test/PropertyTutorial.h"

int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);

    AutoRefreshView view;
    view.setTitle("VerdigrisTest");
    view.setMinimumSize(QSize(400, 600));
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    QSurfaceFormat format = view.format();
    format.setSamples(16);
    view.setFormat(format);

    qmlRegisterType<PropertyTutorial>("types.test", 0, 1, "PropertyTutorial");

#ifdef QT_DEBUG
    QString path(VERDIGRISTEST_PROJECT_FOLDER);
    path += "/src/main.qml";
    view.setSource(QUrl(path));
#else
    view.setSource(QUrl("qrc:/src/main.qml"));
#endif

    view.show();

    return app.exec();
}
