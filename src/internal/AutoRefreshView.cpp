#include "AutoRefreshView.h"

#include <QQmlEngine>
#include <QDebug>
#include <QDirIterator>

#include "config.h"

AutoRefreshView::AutoRefreshView(QWindow *parent) : QQuickView(parent) {
    init();
}

AutoRefreshView::AutoRefreshView(QQmlEngine *engine, QWindow *parent) : QQuickView(engine, parent) {
    init();
}

AutoRefreshView::AutoRefreshView(const QUrl &source, QWindow *parent) : QQuickView(source, parent) {
    init();
}

void AutoRefreshView::init() {
#ifdef QT_DEBUG
    connect(&m_watcher, &QFileSystemWatcher::fileChanged, this, &AutoRefreshView::handleFileChanged);

    QDirIterator it(VERDIGRISTEST_PROJECT_FOLDER, QStringList() << "*.qml", QDir::Files, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        it.next();
        m_watcher.addPath(it.filePath());
        qDebug() << "watching :" << it.filePath();
    }
#endif
}

void AutoRefreshView::refresh() {
#ifdef QT_DEBUG
    qDebug() << "refreshing...";
    QUrl tmp = source();
    setSource(QUrl());
    engine()->clearComponentCache();
    setSource(tmp);
#endif
}

void AutoRefreshView::handleFileChanged(const QString &file) {
    Q_UNUSED(file);
    refresh();
}
