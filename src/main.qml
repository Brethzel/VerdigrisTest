import QtQuick 2.3
import types.test 0.1

Item {
    id: root

    PropertyTutorial {
        id: tutorial
    }

    Component.onCompleted: {
        // This shows properties but all the properties are undefined.
        // It's also impossible to call the WRITE method of properties.
        // Q_INVOKABLE methods works.

        for (var p in tutorial)
            console.log(p + " " + tutorial[p])
    }
}
