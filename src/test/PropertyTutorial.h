#ifndef PROPERTYTUTORIAL_H
#define PROPERTYTUTORIAL_H

#include "../verdigris/wobjectdefs.h"
#include <QObject>
#include <QtCore/QMap>
#include <QDebug>

class PropertyTutorial  : public QObject {
    W_OBJECT(PropertyTutorial)

public:
    /** W_PROPERTY(<type>, <name> [, <flags>]*)

        There are the macro READ WRITE MEMBER and so on which have been defined so
        you can just add a comma after the type, just like in a Q_PROPERTY.

        W_PROPERTY need to be put after all the setters, getters, signals and members
        have been declared.
    */

    QString m_value;
    QString value() const { return m_value; }
    void setValue(const QString &value) {
        qDebug() << "set value called...";
        m_value = value;
        emit valueChanged();
    }
    void valueChanged()
    W_SIGNAL(valueChanged)

//    W_INVOKABLE(setValue, (const QString&))

    // Just like in Qt only with one additional comma after the type
    W_PROPERTY(QString, prop1 READ value WRITE setValue NOTIFY valueChanged)

    // Is equivalent to:
    W_PROPERTY(QString, prop2, &PropertyTutorial::value, &PropertyTutorial::setValue,
               W_Notify, &PropertyTutorial::valueChanged)
    // The setter and getter are matched by signature. add W_Notify before the notify signal

    // By member:
    W_PROPERTY(QString, prop3 MEMBER m_value NOTIFY valueChanged)
    //equivalent to
    W_PROPERTY(QString, prop4, &PropertyTutorial::m_value, W_Notify, &PropertyTutorial::valueChanged)

    // Optionally, you can put parentheses around the type, useful if it contains a comma
    QMap<int, int> m_map;
    W_PROPERTY((QMap<int,int>), map  MEMBER m_map)

};

#endif
